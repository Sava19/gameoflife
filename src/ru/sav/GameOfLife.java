package ru.sav;
import java.util.Random;

public class GameOfLife {

    private int[][] field;
    private int xDimension;
    private int yDimension;

    public static final int PRINT_ALL = 1;
    public static final int PRINT_FINAL = 0;

    public GameOfLife(int xDimension, int yDimension, int countOfCells ) {

        field = new int[yDimension][xDimension];

        this.xDimension = xDimension;
        this.yDimension = yDimension;

        createFirstPopulation(countOfCells);
    }

    public GameOfLife(int dimension, int countOfCells) {

        field = new int[dimension][dimension];

        this.xDimension = dimension;
        this.yDimension = dimension;

        createFirstPopulation(countOfCells);
    }

    private void createFirstPopulation(int countOfCells) {
        if (countOfCells > (xDimension * yDimension)) {
            System.out.println("Cannot create field, number of cells exceed the field dimensions");
        }
        else {

            Random random = new Random();

            for (int i = 0; i < countOfCells; i++) {
                int x = random.nextInt(xDimension);
                int y = random.nextInt(yDimension);

                field[y][x] = 1;

            }


        }

    }

    public void printField() {

        for (int i = 0; i < yDimension; i++) {
            for (int j = 0; j < xDimension; j++) {
                System.out.print(field[i][j] + " ");
            }
            System.out.println();
        }

    }

    public void run(int countOfIteration, int printOption) {

        System.out.println("Start: ");
        this.printField();

        for (int i = 0; i < countOfIteration; i++) {

            int[][] newIterationField = new int[yDimension][xDimension];

            for (int y = 0; y < yDimension; y++) {
                for (int x = 0; x < xDimension; x++) {

                    //check nearby cells
                    int sumOfCells = 0;

                    for (int yy = y - 1; yy <= y + 1 ; yy++) {
                        for (int xx = x - 1; xx <= x + 1; xx++) {

                            if (yy == y && xx == x) {
                                //exclude current cell
                                continue;
                            }
                            //coordinates of nearby cell
                            int nearX =  xx;
                            int nearY = yy;

                            //to connect edges of the field with each other (field is a torus)
                            if (xx < 0) {
                                nearX = xDimension - 1;
                            }

                            if (xx == xDimension) {
                                nearX = 0;
                            }

                            if (yy < 0) {
                                nearY = yDimension - 1;
                            }

                            if (yy == yDimension) {
                                nearY = 0;
                            }

                            sumOfCells += field[nearY][nearX];
                        }
                    }

                    if (sumOfCells == 3) {
                        newIterationField[y][x] = 1;
                    }
                    else {
                        newIterationField[y][x] = 0;
                    }
                }
            }

            //move the new iteration to the main field

            for (int y = 0; y < yDimension; y++) {
                for (int x = 0; x < xDimension; x++) {
                    field[y][x] = newIterationField[y][x];
                }
            }

            if (printOption == PRINT_ALL) {
                System.out.println(i + " iteration:");
                this.printField();
            }
        }

        System.out.println("Final result: ");
        this.printField();
    }
}
